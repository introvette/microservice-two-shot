from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    bin = models.ForeignKey(BinVO, related_name="shoe", on_delete=models.CASCADE)
    picture_url = models.URLField(null=True, blank=True)

    def get_api_url(self):
        return reverse("shoes_detail", kwargs={"pk": self.pk})
