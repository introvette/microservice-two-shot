from django.urls import path
from .views import shoes_detail, shoes_list, bin_list

urlpatterns = [
    path("shoes/", shoes_list, name="shoes_list"),
    path("shoes/<int:pk>/", shoes_detail, name="shoes_detail"),
    path("bin/", bin_list, name="bin_list"),
]
