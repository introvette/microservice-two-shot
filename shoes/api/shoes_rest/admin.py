from django.contrib import admin

from .models import Shoes, BinVO


@admin.register(Shoes)
class ShoeAdmin(admin.ModelAdmin):
    pass


@admin.register(BinVO)
class BinVoAdmin(admin.ModelAdmin):
    pass
