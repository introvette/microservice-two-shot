from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from .models import Shoes, BinVO
from common.encoders import ShoesListEncoder, ShoeDetailEncoder, BinVODetailEncoder


@require_http_methods(["GET", "POST"])
def shoes_list(request):

    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoesListEncoder, safe=False)

    else:

        content = json.loads(request.body)

        try:
            import_href = content["bin"]
            bin = BinVO.objects.get(import_href=import_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin id"},
                status=400,
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def shoes_detail(request, pk):
    try:
        shoes = Shoes.objects.get(pk=pk)
    except Shoes.DoesNotExist:
        return JsonResponse({"error": "Shoe does not exist"}, status=404)

    if request.method == "GET":
        return JsonResponse({"shoes": shoes}, encoder=ShoeDetailEncoder, safe=False)

    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"error": "does not exist"}, status=400)
        Shoes.objects.filter(id=pk).update(**content)
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse({"shoes": shoes}, encoder=ShoeDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0}, safe=False)


@require_http_methods(["GET"])
def bin_list(request):
    bins = BinVO.objects.all()
    return JsonResponse({"bins": bins}, encoder=BinVODetailEncoder, safe=False)
