from shoes_rest.models import BinVO, Shoes
from common.json import ModelEncoder


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name", "bin_number", "bin_size"]


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["manufacturer", "model_name", "color", "id", "picture_url"]

    encoders = {"bin": BinVODetailEncoder()}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = ["manufacturer", "model_name", "color", "picture_url", "bin", "id"]

    encoders = {"bin": BinVOEncoder()}
