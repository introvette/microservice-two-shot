from hats_rest.models import LocationVO, Hats
from common.json import ModelEncoder


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ['import_href', 'closet_name', 'section_number', 'shelf_number']

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ['closet_name']

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = ['fabric', 'style_name', 'color', 'picture_url', 'id']

    encoders = {'location': LocationVODetailEncoder() }

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = ['fabric', 'style_name', 'color', 'picture_url', 'location', 'id']

    encoders = { 'location': LocationVOEncoder() }
