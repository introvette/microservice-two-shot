from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.CharField(max_length=100, blank=True, null=True, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveIntegerField(blank=True, null=True)
    shelf_number = models.PositiveIntegerField(blank=True, null=True)

class Hats(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True, blank=True)
    location = models.ForeignKey(LocationVO, related_name='hats', on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("hats_detail", kwargs={"pk": self.pk})
