import React from 'react'


async function Delete(id) {
    const url = "http://localhost:8080/api/shoes/" + id;
    const fetchConfig = {
        method: "DELETE",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        window.location.reload(false);
    }
} // made class to get state to map in div to access props
class ShoesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shoes: []
        };
    } ///The componentDidMount method allows us to execute the React code when the component is already placed in the DOM (Document Object Model).
    async componentDidMount() {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            this.setState({ shoes: data.shoes });
            console.log(data.shoes)
        }
    }
    render() {
        return (
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Shoe Model</th>
                            <th>Shoe Color</th>
                            <th>Stock Photo</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.shoes.map(shoe => {
                            return (
                                <tr key={shoe.href}>
                                    <td>{shoe.model_name}</td>
                                    <td>{shoe.color}</td>
                                    <td><img src={shoe.picture_url} alt="shoe" width="50" height="50" /></td>
                                    <td><button onClick={() => Delete(shoe.id)}>Delete</button></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <button><a href="http://localhost:3000/shoes/new/">Create a shoe</a></button>
            </div>
        )
    }
}
export default ShoesList;
